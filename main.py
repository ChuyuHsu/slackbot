#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, os, datetime, time, re
import multiprocessing
import random
from slackbot import bot
import logging
import search
import google

logging.basicConfig(level=logging.DEBUG)

# 請預先將主辦單位分發的 Bot token 設成環境變數，以避免放置在程式中有外流之疑慮
# TOKEN_MYBOT = os.environ['MY_BOT']
bot.settings.API_TOKEN  = "xoxb-58453275287-7y7k2w6yOry7BHuWayclxkBx"
# bot.settings.API_TOKEN  = "xoxb-60316845219-Z5J7TUrVR6tYkWuoavrbjEPP"
# testonetwothree 的 Channel ID
CHANNEL_ID = "C20QYLSKE"
# 算分 Bot 的 ID
PIX_INSPECTOR = "U1QDCHJ3H"
bot.settings.DEFAULT_REPLY = "Sorry but I didn't understand you"

from slackbot.bot import Bot
from slackbot.bot import respond_to
from slackbot.bot import listen_to

def main():
    bot = Bot()
    bot.run()

ANSWER_LIST = []

iterator = search.get_key()

def custom_search(query, return_value):
    question, options_k = query
    o = search.commit(question, options_k, next(iterator))
    return_value['custom'] = o

def page_search(query, return_value):
    question, options_k = query
    answers = list(google.search(question, pause=0.5, num=1, stop=1))
    if answers is not None and len(answers) > 0:
        answer = answers[0]
        for key in options_k.keys():
            if key in answer:
                return_value['page'] = options_k[key]


## 取得 quizmaster 丟出的題目字串，解析出問題及選項
@listen_to("題目" + ' (.*)', re.DOTALL)
def receive_question(message, question_string):
    # if message._client.users[message._get_user_id()][u'name'] == "pix_quizmaster":
    if True:
        m = re.match('\[(\d+)\] (.*)### (.*) \[END\]', question_string)
        quiz_no = int(m.group(1))
        question = m.group(2).strip()
        options = {}
        for item in m.group(3).split(','):
            index, value = item.split(':')
            options[index.strip()] = value.strip()
        # 可在此呼叫自定義算法，透過題目(question)、選項(options)，計算出該題答案，本範例為 random 一個答案
        # print("11: ", question)
        # print("00: ", options)

        options_k = dict((v, k) for k, v in options.items())
        query = (question, options_k)

        manager = multiprocessing.Manager()
        return_dict = manager.dict()
        jobs = []

        p = multiprocessing.Process(target=custom_search, args=(query, return_dict))
        p2 = multiprocessing.Process(target=page_search, args=(query, return_dict))
        jobs.append(p)
        jobs.append(p2)
        p.start()
        p2.start()

        for proc in jobs:
            proc.join(3000)
        print(return_dict)

        custom = return_dict.get("custom")
        page = return_dict.get("page", "c")

        ANSWER_LIST.append(custom if custom is not None else page)

## 當 quizmaster 丟出 "機器人小朋友請搶答"，請儘速把答案丟到 channel
@listen_to("機器人小朋友請搶答" +'$')
def hello_send(message):
    if message._client.users[message._get_user_id()][u'name'] == "pix_quizmaster":
    # if True:
        reply_ans = ""
        for idx, ans in enumerate(ANSWER_LIST):
            reply_ans += str(idx + 1) + " : " + ans + ", "
        message.send("<@%s>: %s %s" % (PIX_INSPECTOR, "請給分 ", reply_ans[:-2]))
        ANSWER_LIST[:] = []

# 幫按讚
@listen_to("題號" + ' (.*)')
def hey(message, ans_string):
    message.react('+1')

@respond_to('github', re.IGNORECASE)
def github(message):
    message.reply('what github? %s' % message)

# 回覆訊息範例
@respond_to('cry', re.IGNORECASE)
def cry_reply(message):
    message.reply('why are you crying?')


if __name__ == "__main__":
    main()
