
# coding: utf-8

# In[1]:

import requests
import random


# In[2]:

url = "https://www.googleapis.com/customsearch/v1?"
url += "key={:s}&"
url += "cx=018185607227299460931:o-q8q9w-a6c&"
url += "&fields=items(snippet)&"
url += "q={:s}"

symbol = "︽⊙＿⊙︽"
ans_index = ["a", "b", "c", "d", "e"]


# In[3]:

key_list = [
    "AIzaSyDXesClAS-9a1CvJB1X2bhllEPS1W-kh3s",
    "AIzaSyAf894MwiNXIJzbGee79nvyLb3ae1Cm5uk",
    "AIzaSyBi3Pv9DAPoJTwfg42i328EslzK1duruR0",
    "AIzaSyCwZOmCZADTD0bJXAzXtd4hAUWGIFDis1s",
    "AIzaSyCcNhNxLUzZpQ9LGUmFCL5zcRDDqplPCYA",
]

def get_key():
    while True:
            yield random.choice(key_list)



# In[5]:

def get_search_result_json(q, k):
    print("use key: ", k)
    query = url.format(k, q)
    try:
        res = requests.get(query, timeout=5)
        r = res.json()
    except Exception as e:
        print(e)
        r = None
    return r


# In[6]:

def commit(q, options, key):
    query = q.replace(symbol, " ")
    json_obj = get_search_result_json(query, key)
    if json_obj == None or "items" not in json_obj: return None

    for item in json_obj["items"]:
        snippet = item["snippet"]
        print(snippet)
        candidate = []
        for k in options.keys():
            if k in snippet: candidate.append(k)
        if len(candidate) == 1:
            return options[candidate[0]]
        candidate2 = []
        for c in candidate:
            index = snippet.index(c)
            segment = c
            if index > 0: segment = snippet[index-1:index+len(c)]
            if index < len(snippet): segment += snippet[index+len(c)]
            if segment in snippet: candidate2.append(c)
        if len(candidate2) == 1:
            return options[candidate2[0]]
    return None
